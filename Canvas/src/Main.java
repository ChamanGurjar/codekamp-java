import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Main{

    MyCanvas canvas;

    public static void main(String arg[]) {

        new Main();
    }

    public Main() {

        JFrame window = new JFrame("GWindow");
        //frame1.setSize(400, 400);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JPanel panel1 = new JPanel();
        window.setContentPane(panel1);

        this.canvas = new MyCanvas();
        this.canvas.setSize(400, 400);

        panel1.add(this.canvas);

        window.pack();
        window.setVisible(true);


        while (true) {
            this.canvas.animate();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

//        JButton button = new JButton("Toggle");
//        button.addActionListener(this);
//        window.add(button);

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        if(this.canvas.brushColor == Color.green) {
//
//            this.canvas.brushColor = Color.red;
//        } else {
//            this.canvas.brushColor = Color.red;
//        }
//        this.canvas.repaint();
//    }

