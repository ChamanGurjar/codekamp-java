import java.awt.*;


public class MyCanvas extends Canvas {


    public Color brushColor = Color.red;
    private int x_cord = 0;
    private int y_cord = 0;

    @Override
    public void paint(Graphics g) {

        //super.paint(g);
        // g.setColor(Color.green);

        g.setColor(brushColor);
        g.fillArc(x_cord, y_cord, 80, 80, 0, 360);
    }

    private void toggleColor() {

        if (this.brushColor == Color.red) {

            this.brushColor = Color.green;
        } else {
            this.brushColor = Color.red;
        }
    }

    private void move() {

        this.x_cord += 10;
        this.y_cord += 10;

    }

    public void animate() {
        this.move();
        this.toggleColor();
        this.repaint();

    }

}
