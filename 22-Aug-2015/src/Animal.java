import java.util.Random;

/**
 * Created by chamangujjar on 22/08/15.
 */
public class Animal {

    public void doAnimalStuff() {

        System.out.println("Animal's Activities");


    }

    public static Animal getRadomAnimal() {

        Random r = new Random();
        int animal = 1 + r.nextInt(4);


        if (animal == 0) {
            return new Cat();

        } else if (animal == 1) {
            return new Dog();
        } else if (animal == 2) {
            return new Elephant();
        } else {
            return new Animal();
        }


    }


}
