/**
 * Created by chamangujjar on 22/08/15.
 */
public class Main {

    public static void main(String args[]) {


        Animal getAnimal = Animal.getRadomAnimal();


        if (getAnimal instanceof Cat) {

            ((Cat) getAnimal).doCatStuff();

        } else  if (getAnimal instanceof Dog) {
            ((Dog) getAnimal).doDogStuff();
        }
        else if (getAnimal instanceof Elephant) {
            ((Elephant) getAnimal).doElephantStuff();
        } else {

            getAnimal.doAnimalStuff();
        }

    }


}
