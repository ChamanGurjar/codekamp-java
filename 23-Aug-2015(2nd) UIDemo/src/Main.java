import javax.swing.*;
import java.awt.*;

/**
 * Created by chamangujjar on 23/08/15.
 */
public class Main {

    public static void main(String[] args) {

//        JFrame firstWindow = new JFrame();
//        firstWindow.setSize(300, 500);
//        firstWindow.setVisible(true);
//
//        JFrame secondWindow = new JFrame();
//        secondWindow.setSize(300, 500);
//        secondWindow.setLocation(300, 300);
//        secondWindow.setVisible(true);


        JFrame Window = new JFrame();
        JPanel myPanel = new JPanel();
        Window.setContentPane(myPanel);


//        Window.setSize(500, 500);
//        Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton myFirstButton = new JButton("Random Button");
        myFirstButton.setSize(100, 50);

        //Window.add(myFirstButton);

        JButton mySecondButton = new JButton("Overlaping First Button");
        mySecondButton.setLocation(400, 400);
        mySecondButton.setSize(100, 50);

        // Window.add(mySecondButton);


        myPanel.add(myFirstButton);
        myPanel.add(mySecondButton);

        JTextField field = new JTextField();
        field.setSize(1000,500);
        field.setText("Hi How Are You");
        myPanel.add(field);


        Window.setVisible(true);


    }

}

