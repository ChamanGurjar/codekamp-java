/**
 * Created by chamangujjar on 12/09/15.
 */
public class Wrestler {

 private String name;
    private  int rank;
    private int matchesWon;
    private int matchesLost;
    private double height;
    private double weight;


    public Wrestler(String name, int rank, int matchesWon, int matchesLost, double height, double weight) {
        this.name = name;
        this.rank = rank;
        this.matchesWon = matchesWon;
        this.matchesLost = matchesLost;
        this.height = height;
        this.weight = weight;
    }





}
