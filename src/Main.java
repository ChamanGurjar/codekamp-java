/**
 * Created by chamangujjar on 25/06/15.
 */
public class Main {
    public static void main(String[] args){

        ArrayStack stack1 = new ArrayStack();

        stack1.push("HI");
        stack1.push("Hello");
        stack1.push("How Are You");
        stack1.push("Welcome");

        System.out.println(stack1.pop());
        System.out.println(stack1.pop());

        stack1.slideOut();
        System.out.println(stack1.slideOut());

        //stack1.slideIn("Welcome");
        //System.out.println(stack1.slideIn());


    }
}
