public class  ArrayStack implements Stack {

    private Object[] StackArray = new Object[100];
    private int top = -1;


    @Override
    public Object pop() {
        if(this.isEmpty()){
            return null;
        }

       Object temp = StackArray[top];
        StackArray[top] = null;
        top--;
        return temp;



    }

    @Override
    public void push(Object o) {
        top++;
        StackArray[top]=o;
    }

    @Override
    public boolean isEmpty() {
        if(top == -1){

            return true;
        }


        return false;
    }

    @Override
    public void slideIn(Object o) {

        Object temp = StackArray[0];
        for(int i = top; i <=0; i--){
            StackArray[i+1] = StackArray[i];
        }
        top++;
        StackArray[0] = o;

    }

    @Override
    public Object slideOut() {
        if(this.isEmpty()){
            return null;
        }
       Object temp = StackArray[0];
        for(int i = 0; i<top; i++){
            StackArray[i] = StackArray[i + 1];
        }
        top --;
        return  temp;
    }

}