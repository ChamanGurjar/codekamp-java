/**
 * Created by chamangujjar on 26/06/15.
 */
public interface Stack {

    public  Object pop();
    public void push(Object o);
    boolean isEmpty();

    public void slideIn(Object o);

    public Object slideOut();



}
