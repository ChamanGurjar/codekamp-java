import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Duplicate implements ActionListener {

    int windowsCount = 0;


    public void create() {
        this.windowsCount++;

        System.out.println("Button Clicked");

        JFrame duplicateWindow = new JFrame("Window number " + this.windowsCount);
        duplicateWindow.setSize(400, 300);
        duplicateWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JPanel pannel = new JPanel();
        duplicateWindow.setContentPane(pannel);


        JLabel label1 = new JLabel("Tap button to see what they do");
        label1.setForeground(Color.cyan);
        pannel.add(label1);

        JButton duplicateButton = new JButton("Duplicate ( " + this.windowsCount + " ) ");
        pannel.add(duplicateButton);
        duplicateButton.addActionListener(this);

        JButton changeButton = new JButton("Change Color");
        pannel.add(changeButton);
        changeButton.addActionListener(this);

        duplicateWindow.setVisible(true);


    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        this.create();
    }
}
