/**
 * Created by chamangujjar on 20/09/15.
 */
public class Main {
    public static void main(String[] args) {

        List myList = new ArrayList();

        myList.add("hello world");
        myList.add("I love Java");
        myList.add("Programming is fun");

        myList.remove(1);

        System.out.println(myList);
    }
}
