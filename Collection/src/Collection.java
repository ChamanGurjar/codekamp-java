/**
 * Created by chamangujjar on 20/09/15.
 */
public interface Collection {

    public boolean isEmpty();

    public void add(Object o);

    public void remove(Object o);

    public int size();

    public void clear();

    public Object[] toArray();

//    public void add(Object o) {
//        this.add(o, this.size());
//    }


}
