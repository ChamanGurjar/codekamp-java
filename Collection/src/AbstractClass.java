/**
 * Created by chamangujjar on 20/09/15.
 */
public abstract class AbstractClass implements List {

    @Override
    public void add(Object o) {
        this.add(o , this.size());
    }





    public void clear() {

        for(int i =0 ; i < this.size() ; i++) {
            this.remove(i);
        }
    }




    public boolean isEmpty() {

        return this.size() <= 0;
    }


    @Override
    public String toString() {
        String myString = "";
        for (int i = 0; i < this.size(); i++) {
            myString +=  this.get(i).toString() + "\n";
        }

        return myString;
    }
}
