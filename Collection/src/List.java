/**
 * Created by chamangujjar on 20/09/15.
 */
public interface List extends  Collection {


    public void add(Object o , int index);

    public void remove(int index);

    public void set(Object o , int i);

    public Object get(int i);




}
