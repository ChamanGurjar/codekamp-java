/**
 * Created by chamangujjar on 20/09/15.
 */
public class ArrayList extends AbstractClass {

    private Object[] itemsArray = new Object[100];
    private int count = 0;

    @Override
    public void add(Object o, int index) {

        if (index > this.size() || index < 0) {
            index = this.size();
        }

        this.shiftUp(index);

        this.itemsArray[index] = o;

        this.incrementSize();

    }

    @Override
    public void remove(int index) {
        if(index >= this.size() || index < 0) {
            return;
        }

        this.shiftDown(index);
        this.decrementSize();
    }

    @Override
    public void set(Object o, int index) {
        if (index > this.size() || index < 0) {
            index = this.size();
        }

        this.itemsArray[index] = o;


    }

    @Override
    public Object get(int i) {

        if(i >= this.size()) {
            return null;
        }

        return this.itemsArray[i];

    }

    @Override
    public void remove(Object o) {
        for (int i = 0; i < this.size(); i++) {
            if(this.itemsArray[i] == o) {
                this.remove(i);
            }
        }
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    private void shiftUp(int index) {

        for (int i = this.size(); i > index; i--) {
            this.itemsArray[i] = this.itemsArray[i - 1];
        }

        this.itemsArray[index] = null;

    }

    private void shiftDown(int index) {
        for(int i = index; i < this.size() - 1; i++) {
            this.itemsArray[i] = this.itemsArray[i + 1];
        }

        this.itemsArray[this.size() - 1] = null;
    }

    private void incrementSize() {
        this.count++;
    }

    private void decrementSize() {
        this.count--;
    }
}
