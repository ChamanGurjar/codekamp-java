/**
 * Created by chamangujjar on 15/08/15.
 */
public class Fighter {

    String name;
    int strength;
    int energy = 100;

    int pol = 10;
    int rem = 15;

    public Fighter(String name, int strength) {

        this.name = name;
        this.strength = strength;
    }

    public int Punch(Fighter opponent) {

        opponent.energy = opponent.energy - this.strength;
        return energy;
    }

}
