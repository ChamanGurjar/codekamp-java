/**
 * Created by chamangujjar on 23/08/15.
 */
public class HardCoded implements BulletPointInterface {

    String[] data = {"10,20,30"} ;


    @Override
    public String[] giveData() {
        String[] array = {"Hi" , "How" , "Are", "You"};
        return array;
    }

    @Override
    public void dance() {
        System.out.println("Hard coded dance");

    }
}
